tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : ((e=expr)               {}
        | ^(PRINT e=expr)         {drukuj ($e.text + " = " + $e.out.toString());}
        | ^(VAR id=ID)            {globals.newSymbol($id.text);}
        | ^(PODST id=ID   e=expr) {globals.setSymbol($id.text, $e.out);}
        )*;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;} 
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ID                       {$out = globals.getSymbol($ID.text);}
        | INT                      {$out = getInt($INT.text);}
        ;
