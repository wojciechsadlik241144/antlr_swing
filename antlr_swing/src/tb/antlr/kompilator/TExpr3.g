tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer scope = 1;
}
prog    : (e+=blok | e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d});

blok    : ^(BEGIN {scope = enterScope();} (e+=blok | e+=expr | d+=decl)* {scope = leaveScope();}) -> blok(name={$e},deklaracje={$d})
        ;

decl  :
        ^(VAR i1=ID) {locals.newSymbol($ID.text);} -> dek(n={$ID.text},s={scope.toString()})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> sub(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st},p2={$e2.st}) 
        | ^(PODST i1=ID   e2=expr) -> mov(i1={$i1.text},p2={$e2.st},s={scope.toString()})
        | INT                      -> int(i={$INT.text})
        | ID                       -> id(n={$ID.text},s={scope.toString()})
    ;
    